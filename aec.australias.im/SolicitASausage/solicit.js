var ipJson = "undef";
var fingerJson = "undef";
var dataJson = "undef";
var state = "undef";
var id = "undef";

function solicitSausage(){
  var db = firebase.database();
  locatorRef = db.ref('solicits/'+state);
  locatorRef.push({
    id: id,
    username: $("#sas-username").val(),
    ip: ipJson,
    finger: fingerJson,
    data: dataJson,
    prerego: $("#sas-prerego").is(':checked'),
    seen: "n"
  }, function(){
    $("h2").html("Our servers have received your submission. Thank you.");
  });
}

$("input[name=region]:radio").change(function () {
  state = $(this).attr('id').substring(0,3);
  id = $(this).attr('id').substring(3,4);
});

$(document).ready(function(){
  try{
    new Fingerprint2().get(function(result, components){
      fingerJson = result;
      dataJson = components;
    });
  }
  catch(err){
   fingerJson = "error";
   console.log("Error: could not get fingerprint");
   dataJson = "error";
  }

  try{$.getJSON("http://jsonip.com/?callback=?", function (data) {ipJson = data.ip;});}
  catch(err){
    ipJson = "error";
    console.log("Error: could not get IP");
  }
});

$("#sas-submit").click(function(){
  if($("#sas-username").val() == ""){$("#sas-username").css("border-color", "#f00");}
  else{
    $("#sas-username").css("border-color", "#333");
    if(id == "undef"){$("#sas-reg-select").css("color", "#f00");}
    else{$("#sas-reg-select").css("color", "#333");
    $("#sas-main").hide();
    $("h2").html("If you see this for more than 10 seconds, please refresh.");
    solicitSausage();
  }
  }


});
