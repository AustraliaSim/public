<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="theme-color" content="#42b2d1">
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<title>AustraliaSim | Explanatory Memorandum Generator</title>
	<link rel="icon" type="image/png" href="./favicon-16x16.png" />
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="style.css">
	
	<base href="/">
</head>
<body style="background: #d0ebf2;">

<?php include ('nav.php'); ?>
		
		<!-- BODY CONTENT START -->
			<div class="container-fluid" style="width:100%;"><div style="padding-bottom:20px;"></div>
				<div class="row">
				<!-- RIGHT CONTAINER START-->
					<div class="col-sm-12 intrinsic-container" style="padding: 0 20%;">
						<h3>Explanatory Memorandum Generator</h3><hr class="aussimhr">
						<p>Use the fields below to input the text of your bill.</p>
						
						<div style="padding:20px;background-color: rgba(250,250,250,0.95);border-radius: 12px;">
						<p style="text-align:right;">Explanatory Memorandum Generator &copy; <a href="https://www.reddit.com/r/TheSpade/comments/4tv6ne/the_spades_automatic_explanatory_memorandum/">RunasSudo</a><br/></p>
		
							Long title: A bill to <input id="longtitle" type="text" value="reform the law relating to taxation" style="width:30em;max-width:75%;">, and for related purposes
							
							<ul id="list">
								<li>
									<input style="max-width:100%;" type="text" class="title" value="Clause 1: Short title"><br>
									<textarea class="body" style="width:100%;;">This bill may be cited as the Taxation Reform Bill 2016.</textarea>
								</li>
								<li>
									<input style="max-width:100%;" type="text" class="title" value="Clause 2: Commencement"><br>
									<textarea class="body" style="width:100%;">This bill commences on the day it receives the Royal Assent.</textarea>
								</li>
								<li>
									<input style="max-width:100%;" type="text" class="title" value="Clause 3: Schedules"><br>
									<textarea class="body" style="width:100%;;">Each Act that is specified in a Schedule to this bill is amended or repealed as set out in the applicable items in the Schedule concerned, and any other item in a Schedule to this bill has effect according to its terms.</textarea>
								</li>
							</ul>
							
							<div style="text-align:center;"><input style="max-width:50%;" type="button" onclick="addSection();" value="Add section">&nbsp;<input style="max-width:50%;" type="button" onclick="generate();" value="Generate explanatory memorandum"></div>
							
							<div id="em"></div>
						</div>
						<div style="margin-bottom:10px;"></div>
					</div>
				<!-- RIGHT CONTAINER END-->
				</div>
			</div>
		<!-- BODY CONTENT END-->
		
		<footer>
			Copyright 2017 <a href="//australias.im/">AustraliaSim</a> - Last updated <script type="text/javascript">document.write(document.lastModified);</script>
		</footer>


</body>

<script>
			function addSection() {
				document.getElementById('list').innerHTML += '<li><input type="text" class="title" value="Clause 4: Section title"><br><textarea class="body" style="width:50em;">Section body</textarea></li>';
			}
			
			function generate() {
				var titles = document.querySelectorAll('.title');
				var bodies = document.querySelectorAll('.body');
				
				var text = '';
				
				text += '<h2>Summary</h2>';
				text += 'This bill aims to ' + document.getElementById('longtitle').value + '.';
				text += '<h2>Financial Impact Statement</h2>';
				text += 'This bill is expected to have no impact on the budget.';
				text += '<h2>Statement of Compatibility with Human Rights</h2>';
				text += 'This bill is compatible with the human rights and freedoms recognised or declared in the international instruments listed in section 3 of the Human Rights (Parliamentary Scrutiny) Act 2011.';
				
				text += '<h2>Notes on Clauses</h2>';
				
				for (var i = 0; i < titles.length; i++) {
					text += '<h3>' + titles[i].value + '</h3>';
					text += '<p>This clause provides that ' + bodies[i].value[0].toLowerCase() + bodies[i].value.substring(1) + '</p>';
				}
				
				document.getElementById('em').innerHTML = text;
			}
</script>


</html>
