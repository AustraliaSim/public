<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="theme-color" content="#42b2d1">
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<title>AustraliaSim | Acts</title>
	<link rel="icon" type="image/png" href="./favicon-16x16.png" />
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="style.css">
	
	<base href="/">
</head>
<body style="background: #d0ebf2;">

<?php include ('nav.php'); ?>
		
		<!-- BODY CONTENT START -->
			<div class="container-fluid" style="width:100%;"><div style="padding-bottom:20px;"></div>
				<div class="row">
				<!-- RIGHT CONTAINER START-->
					<div class="col-sm-12 intrinsic-container" style="padding: 0 20%;">
						<h3>Acts</h3><hr class="aussimhr">
						<p>This site is still under construction, feel free to have a look around though!</p>
						
						<div style="padding:20px;background-color: rgba(250,250,250,0.95);border-radius: 12px;">
							<table class="table">
								<thead>
								  <tr>
									<th width="30%">Title</th>
									<th width="45%">Description</th>
									<th width="12.5%" style="text-align:center;">Status</th>
									<th width="12.5%" style="text-align:center;">View/Download</th>
								  </tr>
								</thead>
								<tbody>
								  <tr class="success">
									<td>Code of Conduct Act 2017</td>
									<td>This Act aims to prescribe standards regulating the conduct of AustraliaSim members, and provide penalties for breaches of those standards in a procedurally fair manner. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/CoCv3.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/CoCv3.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>Medical Services (Dying with Dignity) Bill 2017</td>
									<td>This bill puts in place the means for those with terminal illnesses to be assisted by medical practitioners to die with dignity. The qualifications for this are rigorously tested, the involvement of multiple medical practitioners provide the greatest level of counselling relevant to provision of services. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/DwD.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/DwD.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>AustraliaSim Constitution Act 2017</td>
									<td>A Bill for an Act to constitute the Commonwealth of AustraliaSim. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/constitution.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/constitution.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>E-Sports Trial Act 2017</td>
									<td>E-Sports Trial Act 2017. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/e-sports.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/e-sports.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>Gambling Advertising Restrictions Act 2017</td>
									<td>A Bill for an Act to restrict advertising of gambling services. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/GAR.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/GAR.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>Commonwealth Electoral Act 2017</td>
									<td>A Bill for an Act to consolidate the law relating to parliamentary elections, and for related purposes. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/CEA.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/CEA.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>Appropriation (Parliamentary Departments) Act (No. 1) 2017-2018</td>
									<td>A Bill for an Act to appropriate money out of the Consolidated Revenue Fund for expenditure in relation to the Parliamentary Departments, and for related purposes. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/PAv1.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/PAv1.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="warning">
									<td>Code of Conduct Amendment Act 2017</td>
									<td>This Act aims to prescribe standards regulating the conduct of AustraliaSim members, and provide penalties for breaches of those standards in a procedurally fair manner. </td>
									<td style="text-align:center;">Superseded</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/CoCv2.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/CoCv2.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="warning">
									<td>Constitutional Amendments (Omnibus) Act 2017</td>
									<td>A Bill for an Act to alter the AustraliaSim Constitution to improve the good governance of Australia, and for related purposes. </td>
									<td style="text-align:center;">Superseded</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/omnibus.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/omnibus.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="warning">
									<td>HomeBroadband (Code of Conduct) Act 2017</td>
									<td>This Act aims to prescribe standards regulating the conduct of AustraliaSim members, and provide penalties for breaches of those standards in a procedurally fair manner. </td>
									<td style="text-align:center;">Superseded</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/CoCv1.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/CoCv1.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>Parliamentary Consultative Body Act 2017</td>
									<td>The purpose of this bill is to engage all members of the sim regardless of whether they are elected representatives or not. This bill will allow members and ministers to develop more comprehensive policy with the assistance of citizen experts.</td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/parliamentary-consultative-body-act.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/parliamentary-consultative-body-act.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>Marriage (Gender Equality) Amendment Act 2017</td>
									<td>A Bill for an Act to amend the Marriage Act 1961, and for related purposes</td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/marriage-equality-act.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/marriage-equality-act.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>Medical Safety Act 2017</td>
									<td>A Bill for an Act to devolve federal control over certain areas of healthcare to local communities. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/MSA.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/MSA.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>Freedom of Speech Act 2017</td>
									<td>A Bill for an Act to amend the Racial Discrimination Act 1975, and for related purposes. </td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/FoS.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/FOS.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="success">
									<td>The below are examples:</td>
									<td></td>
									<td style="text-align:center;"></td>
									<td style="text-align:center;"></td>
								  </tr>
								  <tr class="success">
									<td>Passed Act 20XX</td>
									<td>This aims to do something, maybe</td>
									<td style="text-align:center;">In force</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/passed.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/passed.pdf" type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="warning">
									<td>Superseded Act 20XX</td>
									<td>This has been modified to do something, maybe</td>
									<td style="text-align:center;">Superseded</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/superseded.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/superseded.pdf"  type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								  <tr class="danger">
									<td>Repealed Act 20XX</td>
									<td>This used to do something, maybe</td>
									<td style="text-align:center;">Repealed</td>
									<td style="text-align:center;"><a href="https://docs.google.com/viewer?url=http://legislation.australias.im/docs/acts/repealed.pdf" target="_blank" type="button" class="btn btn-primary btn-xs">View</a>&nbsp;<a href="http://legislation.australias.im/docs/acts/repealed.pdf"  type="button" class="btn btn-danger btn-xs" download>Download</a></td>
								  </tr>
								</tbody>
							</table>
						</div>
						
					</div>
				<!-- RIGHT CONTAINER END-->
				</div>
			</div>
		<!-- BODY CONTENT END-->
		
		<div style="padding-bottom:20px;"></div>
		
		<footer>
			Copyright 2017 <a href="//australias.im/">AustraliaSim</a> - Last updated <script type="text/javascript">document.write(document.lastModified);</script>
		</footer>


</body>
</html>
