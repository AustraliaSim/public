<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="theme-color" content="#42b2d1">
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<title>AustraliaSim | Legislation</title>
	<link rel="icon" type="image/png" href="./favicon-16x16.png" />
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="style.css">
	
	<base href="/">
</head>
<body style="background: #d0ebf2;">

<?php include ('nav.php'); ?>
		
		<!-- BODY CONTENT START -->
			<div class="container-fluid" style="width:100%;"><div style="padding-bottom:20px;"></div>
				<div class="row">
				<!-- RIGHT CONTAINER START-->
					<div class="col-sm-12 intrinsic-container" style="padding: 0 20%;">
						<h3>Register of Legislation</h3><hr class="aussimhr">
						<p>This site is still under construction, feel free to have a look around though!</p>
					</div>
				<!-- RIGHT CONTAINER END-->
				</div>
			</div>
		<!-- BODY CONTENT END-->
		
		<footer>
			Copyright 2017 <a href="//australias.im/">AustraliaSim</a> - Last updated <script type="text/javascript">document.write(document.lastModified);</script>
		</footer>


</body>
</html>
