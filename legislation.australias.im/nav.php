<link href="https://fonts.googleapis.com/css?family=Varela+Round|Fredoka+One" rel="stylesheet">
<div id="navigation-main" class="navbar navbar-default navbar-fixed-top">
	<div class="container" >
		<div class="navbar-header">
			<a href="/home" class="navbar-brand">
				<h1 height="36">AustraliaSim Legislation</h1>
			</a>
			<button type="button" data-toggle="collapse" data-target="#navbar-main" class="navbar-toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="navbar-main" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<!--<li><a href="/index.php">Home </a></li>-->
				<li><a href="/constitution">Constitution </a></li>
				<li><a href="/acts">Acts </a></li>
				<li><a href="/generator">Generator </a></li>
				<!--<li><a href="/bills.php">Bills</a></li>-->
			</ul>
		</div>
	</div>
</div>